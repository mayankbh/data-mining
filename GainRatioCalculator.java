import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
class GainRatioCalculator
{
	int lineNumber=0;
	String keyValueMatrix[][]=new String[1000][4];	
	int labelCount[]=new int[100];
	String majorityLabel=null;
	String path;
	
	public String majorityLabel()
	{
		return majorityLabel;
	}
	
	
	//Calculation of entropy
	public double calculateCurrentNodeEntropy()
	{
		int firstIndex;
		firstIndex=Integer.parseInt(keyValueMatrix[0][0]);	//Set current index to index of first file entry
		int i=0;
		int recordHandled[]=new int[1000];
		String classLabel=keyValueMatrix[0][2];
		int j=0;
		int classLabelIndex=-1;
		int maxCount=0;

		
		//It's just to calculate the entropy of the current node
		//Take first entry, all entries corresponding to that column index are
		//consecutive, hence keep going and counting the class label values
		while(firstIndex==Integer.parseInt(keyValueMatrix[j][0]))	
		{
			if(recordHandled[j]==0)
		    {
				classLabel=keyValueMatrix[j][2];
				classLabelIndex++;
		        i=j;
		        //Iterating over all entries with same attribute index
				while(firstIndex==Integer.parseInt(keyValueMatrix[i][0]))
				{   					
					if(recordHandled[i]==0)
				  	{
						if(classLabel.contentEquals(keyValueMatrix[i][2]))
			  			{
							labelCount[classLabelIndex]=labelCount[classLabelIndex]+Integer.parseInt(keyValueMatrix[i][3]);
			  				recordHandled[i]=1;
			  			}
				  	}
		  			i++;
		  			if(i==lineNumber)	//EOF
						  break;
				}
				
				if(labelCount[classLabelIndex]>maxCount)
				{
					  maxCount=labelCount[classLabelIndex];	//Update most frequent class label
					  majorityLabel=classLabel;
				}
		    }
			else
			{
				j++;
			}
			if(j==lineNumber)		//EOF
				break;
		  
		}
		return entropy(labelCount);
		  
	  }
	
	  public double entropy(int countArray[])
	  {
		   
		  	double entropy=0;
			int i=0;
			int sum=0;
			double fractional;
			while(countArray[i]!=0)
			{
				sum=sum+countArray[i];
				i++;
			}
			i=0;
			while(countArray[i]!=0)
			{
				fractional=(double)countArray[i]/sum;
		  
				entropy=entropy-fractional*(Math.log(fractional)/Math.log(2));
				i++;
			}
		
			return entropy;
	  }
	  
	  public void getCountFromHDFS()
	  { 
		  try 
		  {	
			  Configuration conf = new Configuration();
			  FileSystem fs = FileSystem.get(conf);

			  FSDataInputStream in = fs.open(new Path(path + "intermediate"+C45.currentQueueIndex+".txt"));

			  
			  String line;
			  //Read File Line By Line
			  StringTokenizer iterator;
			  // System.out.println("READING FROM intermediate  "+id.current_index);
		  
			  while ((line = in.readLine()) != null)   
			  {
				  iterator= new StringTokenizer(line);
				
				  keyValueMatrix[lineNumber][0]=iterator.nextToken();	//Attribute index
				  keyValueMatrix[lineNumber][1]=iterator.nextToken();	//Value
				  keyValueMatrix[lineNumber][2]=iterator.nextToken();	//Label
				  keyValueMatrix[lineNumber][3]=iterator.nextToken();	//Count of the tuple
			
				  lineNumber++;
			  }

			  keyValueMatrix[lineNumber][0]=null;
			  keyValueMatrix[lineNumber][1]=null;
			  keyValueMatrix[lineNumber][2]=null;
			  keyValueMatrix[lineNumber][3]=null;
			  in.close();
		  
		  }
		  catch (Exception e) 
		  {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
	    	  
	    	  
		  }  
	  }
	  
	  
	  public double gainratio(int index,double entropy)
	  {
		  //Index-Value-Label-Count quadruples
		  //100 is considered as max ClassLabels
		
		  int countMatrix[][]=new int[1000][100];
		  int sum[]=new int[1000]; //
		  String currentAttributeValue="@5289fhkt";	//Take a junk string which no attribute value will match
		  double gainRatio=0;
		  int j=0;
		  int m=-1;  //index for split number 

		  int totalSum=0;
		  for(int i=0;i<lineNumber;i++)	//Iterate over the entries in file
		  {
			  if(Integer.parseInt(keyValueMatrix[i][0])==index)	//Check if matches attribute
			  {	  
	    		  if(keyValueMatrix[i][1].contentEquals(currentAttributeValue))
	    		  {
	    			  j++;
	    			  countMatrix[m][j]=Integer.parseInt(keyValueMatrix[i][3]);
	    			  sum[m]=sum[m]+countMatrix[m][j];
	    		  }
	    		  else
	    		  {
	    			  j=0;
	    			  m++;
	    			  currentAttributeValue=keyValueMatrix[i][1];
	    			  countMatrix[m][j]=Integer.parseInt(keyValueMatrix[i][3]); //(different class) data sets count per m index split
	    			  sum[m]=countMatrix[m][j];
	    		  }
			  }
		  }
		  int p=0;
		  while(sum[p]!=0)
		  {
			  totalSum=totalSum+sum[p]; //calculating total instances in node
			  p++;
		  }
		
		  double splitEntropy=0;
		  double splitInfo=0;
		  double fractionalPart=0;
		  for(int splitnum=0;splitnum<=m;splitnum++)
		  {
			  fractionalPart=(double)sum[splitnum]/totalSum;
			  splitEntropy=splitEntropy+fractionalPart*entropy(countMatrix[splitnum]);	//Applying the formula
		  }
		  splitInfo=entropy(sum);
		  gainRatio=(entropy-splitEntropy)/(splitInfo);
		  return gainRatio;
	  }
	  
	  
	  public String getvalues(int n)
	  {   
		  //Returns all possible unique values an attribute at index n can take
		  int flag=0;
		  String values="";
		  String temp="%072gagrASSD@";	//Junk string that can never be matched
		  for(int z=0;z<1000;z++)
		  {
			  if(keyValueMatrix[z][0]!=null)		//Not EOF
			  {
				  if(n==Integer.parseInt(keyValueMatrix[z][0]))	//If attribute index matches
				  {
					  flag=1;	//Flag the attribute as already seen
				
					  if(!keyValueMatrix[z][1].contentEquals(temp))
					  {
						  values=values+" "+keyValueMatrix[z][1];	//Append attribute value to values
						  temp=keyValueMatrix[z][1];		//temp is the last seen attribute value
					  }
				  }
				  else if(flag==1)	//Required attribute was seen earlier, terminate
					  break;
			  }
			else		//EOF
				break;
		  }
		  return values;			 
	  }	  
}
	  
