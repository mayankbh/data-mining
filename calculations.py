import sys

#Accuracy, precision, recall

P_P = 1
P_N = 2
N_P = 3
N_N = 4

def main():
	if len(sys.argv) != 3:
		print "Usage : calculations.py <record file> <rule file>"
		exit(1)
	input_file = open(sys.argv[1])
	rule_file = open(sys.argv[2])

	perform_calculations(input_file, rule_file)
	
def perform_calculations(input_file, rule_file):
	records = input_file.readlines()
	rules = rule_file.readlines()
	p_p = 0
	p_n = 0
	n_p = 0
	n_n = 0
	for record in records:
		temp = test_classify(record, rules)
		if(temp == P_P):
			print "True positive"
			p_p = p_p + 1
		elif(temp == P_N):
			print "False negative"
			p_n = p_n + 1
		elif(temp == N_P):
			print "False positive"
			n_p = n_p + 1
		else:
			print "True negative"
			n_n = n_n + 1
	
	accuracy =  (float(p_p + n_n)) / (p_p + n_n + p_n + n_p)
	print "Accuracy = " + str(accuracy)

def test_classify(record, rules):
	print "For record " + record
	for rule in rules:
		print "Checking rule " + rule
		if record_matches_rule(record, rule):
			print "Found matching rule" + rule
			return check_classification(record, rule)

def record_matches_rule(record_as_string, rule_as_string):
	rule = rule_as_string.split()	#Convert to list
	record = record_as_string.split()
	length_of_rule = len(rule)
	for i in xrange(0, length_of_rule - 2, 2):
		attribute_index = int(rule[i])
		if(record[attribute_index] != rule[i+1]):
			return False

	return True

def check_classification(record, rule):
	actual_class = record.split()[-1]
	rule_class = rule.split()[-1]

	print "Actual class = " + actual_class
	print "Rule class = " + rule_class

	if(rule_class == actual_class and rule_class == 'Y'):
		return P_P
	elif(rule_class == actual_class and rule_class == 'N'):
		return N_N
	elif(rule_class == 'Y'):
		return N_P
	else:
		return P_N

if __name__ == '__main__':
	main()
