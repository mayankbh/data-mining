import java.util.ArrayList;

public class TreeNode
{
	public ArrayList<Integer> attributeIndexList;
	public ArrayList<String> attributeValueList;
	
	String classLabel;
	TreeNode()
	{
		 this.attributeIndexList= new ArrayList<Integer>();
		 this.attributeValueList = new ArrayList<String>();	
	}
	
	TreeNode(ArrayList<Integer> attr_index,ArrayList<String> attr_value)
	{
		this.attributeIndexList=attr_index;
		this.attributeValueList=attr_value;
		
	}
	

}
