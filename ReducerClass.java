import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public  class ReducerClass extends MapReduceBase
implements Reducer<Text, IntWritable, Text, IntWritable> 
{
	
	public static String path;
	
	public void reduce(Text key, Iterator<IntWritable> values,
                   OutputCollector<Text, IntWritable> output,
                   Reporter reporter) throws IOException 
    {
		//Essentially finds the total number of times an Index-Value-Label tuple occurs

		int sum = 0;
		//Essentially a Text object containing Attribute index - Value - Class label
		while (values.hasNext()) 
		{
			sum += values.next().get();
		}
		output.collect(key, new IntWritable(sum));
		//It writes key and sum pairs to the file
		//File now contains Attribute Index-Value-Label-Sum quadruples
		
		writeToFileInHDFS(key + " " + sum);
    }

	public static void writeToFileInHDFS(String text) throws IOException
	{
		
		Configuration conf;
		FileSystem fs;
		FSDataOutputStream out;
			conf = new Configuration();
			fs = FileSystem.get(conf);
		
		try 
		{			
			out = fs.append(new Path(path + "intermediate" + C45.currentQueueIndex + ".txt"));
			
			out.writeBytes(text);
			out.writeByte('\n');
			out.close();
	    } 
		catch (FileNotFoundException e) 
		{
			out = fs.create(new Path(path + "intermediate" + C45.currentQueueIndex + ".txt"));
			out.writeBytes(text);
			out.writeByte('\n');
			out.close();
	    }	
	}
	
	public static void writeToFile(String text) 
{
	try 
	{
    	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path + "intermediate"+C45.currentQueueIndex+".txt"), true));    
    	bw.write(text);
        bw.newLine();
        bw.close();
    } 
	catch (Exception e) 
	{
    }
}

}
