import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;



public class C45 extends Configured implements Tool 
{
	public static TreeNode currentSplit=new TreeNode();
    public static int currentQueueIndex=0;
    public static ArrayDeque<TreeNode> queue = new ArrayDeque<TreeNode>();
    public static ArrayList<String> attributeTitles = new ArrayList<String>();
    public static ArrayList<String> verifyRules = new ArrayList<String>();
    
    public static void main(String[] args) throws Exception 
    { 
    	if(args.length != 4)
    	{
    		System.out.println("args[0]:path to folder containing data\n" +
    				"args[1]:name of input file\n" + 
    				"args[2]:number of attributes\n" + 
    				"args[3]:file containing column names");
    		System.exit(1);
    	}
    	
    	FileReader in = new FileReader(args[0] + args[3]);
    	BufferedReader buf = new BufferedReader(in);
    	String allAttributes = buf.readLine();
    	StringTokenizer attributeTokenizer = new StringTokenizer(allAttributes);
    	while(attributeTokenizer.hasMoreTokens())
    	{
    		attributeTitles.add(attributeTokenizer.nextToken());
    	}
    	in.close();
    	
    	queue.add(currentSplit);
    	int res=0;
    	
    	ArrayList<String> rules = new ArrayList<String>();
    	String outputFilePath = args[0];
    	
		int bestSplitIndex=0;
		double gainRatio=0;
		double bestGainRatioSoFar=0;
		double currentNodeEntropy=0;
		String classLabel=null;
		int totalAttributeCount;
		totalAttributeCount=Integer.parseInt(args[2]);
		GainRatioCalculator gainObj;
		TreeNode newSplittingNode;
		
		
		
	 // While queue still has elements
		while(!queue.isEmpty())
        {
			currentSplit = queue.poll();
			gainObj=new GainRatioCalculator();		
			gainObj.path = args[0];
			
		    res = ToolRunner.run(new Configuration(), new C45(), args);	
    	
		    int j=0;
		    int temporarySize;
		    gainObj.getCountFromHDFS();
		    currentNodeEntropy=gainObj.calculateCurrentNodeEntropy();
		    classLabel=gainObj.majorityLabel();
		    currentSplit.classLabel=classLabel;
	    
		    if(currentNodeEntropy!=0.0 && currentSplit.attributeIndexList.size() != totalAttributeCount)
		    {		    	
		    	bestSplitIndex = -1;
		    	bestGainRatioSoFar=0;

		    	//Loop iterates over attributes and find next attribute
		    	//Which gives best gain ratio for split
			    for(j=0;j<totalAttributeCount;j++)		//Finding the gain of each attribute
			    {
			    	
			    	  if(!currentSplit.attributeIndexList.contains(j))  // Splitting already done with this attribute
			  	      {
			    		  gainRatio=gainObj.gainratio(j,currentNodeEntropy);
				  	    	
			  	    	  if(gainRatio>=bestGainRatioSoFar)
			  	    	  {
			  	    		  //Update best index
			  	    		  bestSplitIndex=j;
			  	    		  //Update gain ratio
			  	    		  bestGainRatioSoFar=gainRatio;
			  	    	  }
			  	      }
			  	      
			    }
			    
			    if(bestSplitIndex == -1)	//No good index to split on
			    {
			    	String newRule="";
			    	String verifyRule = "";
			    	temporarySize=currentSplit.attributeIndexList.size();
			    	//Generate the rule
			    	for(int val=0;val<temporarySize;val++)  
			    	{
			    		verifyRule=verifyRule+" "+currentSplit.attributeIndexList.get(val)+" "+currentSplit.attributeValueList.get(val);
			    		newRule = newRule + " " + attributeTitles.get(currentSplit.attributeIndexList.get(val)) +
			    						" = " + currentSplit.attributeValueList.get(val);
			    	}
			    	newRule=newRule+" -> "+currentSplit.classLabel;
			    	verifyRule = verifyRule + " " + currentSplit.classLabel;
			    	rules.add(new String(newRule));	//Add to list of rules
			    	verifyRules.add(verifyRule);
			    }
			    else
			    {	//Possible index to split on
			    //Get the unique values the best split attribute can take
			    	String attr_values_split=gainObj.getvalues(bestSplitIndex);
			    //Tokenize the set of unique values
			    	StringTokenizer attrs = new StringTokenizer(attr_values_split);
			    //Calculate how many children are made at this split
			    	int number_splits=attrs.countTokens(); //number of splits possible with  attribute selected
			    			    
			    	
			    
			    //Iterate over each split
			    	for(int splitnumber=1;splitnumber<=number_splits;splitnumber++)
			    	{
			    		newSplittingNode=new TreeNode(); 
			    	
			    		newSplittingNode.attributeIndexList.addAll(currentSplit.attributeIndexList);
				    	newSplittingNode.attributeValueList.addAll(currentSplit.attributeValueList);
			    	
			    	
				    	//Insert it into the split
				    	newSplittingNode.attributeIndexList.add(bestSplitIndex);
				    	newSplittingNode.attributeValueList.add(attrs.nextToken());
			    	
				    	//Add this to the list of splits
				  
				    	queue.add(newSplittingNode);
			    	}
			    }
		    }
		    else	//Entropy is zero or we've run out of attributes to split on
		    {		//Write rule to file now
		    	String newRule="";
		    	String verifyRule = "";
		    	temporarySize=currentSplit.attributeIndexList.size();
		    	//Generate the rule
		    	for(int val=0;val<temporarySize;val++)  
		    	{
		    		verifyRule=verifyRule+" "+currentSplit.attributeIndexList.get(val)+" "+currentSplit.attributeValueList.get(val);
		    		newRule = newRule + " " + attributeTitles.get(currentSplit.attributeIndexList.get(val)) +
    						" = " + currentSplit.attributeValueList.get(val);
		    	}
		    	newRule=newRule+" -> "+currentSplit.classLabel;
		    	verifyRule = verifyRule + " " + currentSplit.classLabel;
		    	rules.add(new String(newRule));	//Add to list of rules
		    	verifyRules.add(verifyRule);
		    }		   	
		    currentQueueIndex++;  
        }
	    writeAllRules(rules, outputFilePath + "rule.txt");
	    writeAllRules(verifyRules, outputFilePath + "verify_rule.txt");
		System.exit(res);

    }
    
    
    
    public static void writeAllRules(ArrayList<String> rules, String outputFilePath)
    {
    	try
    	{
    		Configuration conf = new Configuration();
    		FileSystem fs = FileSystem.get(conf);
    		FSDataOutputStream out = fs.create(new Path(outputFilePath));
    	
    		for(String rule : rules)
    		{
    			out.writeBytes(rule);
    			out.writeByte('\n');
    		}
    		
    		out.close();
    	}
    	catch(IOException e)
    	{
    		e.printStackTrace();
    	}
    	
    }
    
    public static void writeRuleToFile(String text, String outputFilePath) 
    {
    	//Writes rule to local filesystem
    	
	    try 
	    {   
	    	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFilePath + "rule.txt"), true));    
	    	bw.write(text);
	        bw.newLine();
	        bw.close();
	    } 
	    catch (Exception e) 
	    {
	    }
	    
	   
	}
  
    public int run(String[] args) throws Exception 
    {
    	
    	JobConf conf = new JobConf(getConf(),C45.class);
    	conf.setJobName("C4.5");
    	conf.setNumReduceTasks(1);
    	// the keys are words (strings)
    	conf.setOutputKeyClass(Text.class);
    	// the values are counts (ints)
    	conf.setOutputValueClass(IntWritable.class);

    	conf.setMapperClass(MapperClass.class);
    	conf.setReducerClass(ReducerClass.class);
    	
    	ReducerClass.path = args[0];
    	
    	FileInputFormat.setInputPaths(conf, args[0] + args[1]);
    	FileOutputFormat.setOutputPath(conf, new Path(args[0] + "output"+currentQueueIndex));

    	JobClient.runJob(conf);	
    	return 0;
    }
    
}
