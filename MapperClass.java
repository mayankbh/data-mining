import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class MapperClass extends MapReduceBase
implements Mapper<LongWritable, Text, Text, IntWritable> 
{

	private final static IntWritable one = new IntWritable(1);
	private Text attValue = new Text();
	private int i;
	private String token;
	public int attributeCount;
	
	public void map(LongWritable key, 
					Text value,
					OutputCollector<Text, IntWritable> output,
					Reporter reporter) throws IOException 
	{
		//Map takes a record and produces 
		//Attribute - Value - Label tuples
		//which lie in the current split
		
		  TreeNode splitToUse=null;
		  int sizeOfSplit=0;
		  splitToUse=C45.currentSplit;	
		  
		  String line = value.toString();      //changing input instance value to string
		  StringTokenizer itr = new StringTokenizer(line);
		  int index=0;
		  String attributeValue=null;
		  attributeCount=itr.countTokens()-1;
		  String attributeValues[]=new String[attributeCount];
		  boolean recordMatchesSplit=true;
		  for(i =0;i<attributeCount;i++)
		  {
			  //Turning attributes into array of Strings
			  attributeValues[i]=itr.nextToken();		//Finding the values of different attributes
		  }
		  //Last entry in record is class label
		  String classLabel=itr.nextToken();	
		  sizeOfSplit=splitToUse.attributeIndexList.size();	//Number of attributes in split
		  for(int count=0;count<sizeOfSplit;count++) //Checks if record is in current split
		  {  
			  index= splitToUse.attributeIndexList.get(count);
			  attributeValue=splitToUse.attributeValueList.get(count);
			  if(!attributeValues[index].equals(attributeValue))   
			  {
				  //Record doesn't match the split
				  recordMatchesSplit=false;
				  break;
			  }
			   
		  }
		  
			
		  if(recordMatchesSplit)		//Record belongs to split
		  {
			  	for(int l=0;l<attributeCount;l++)
			  	{  
			  		if(!splitToUse.attributeIndexList.contains(l))	//Attribute not in split
			  		{
			  			//Write attribute index, attribute value and class label
			  			token=l+" "+attributeValues[l]+" "+classLabel;	
			  			attValue.set(token);
			  			output.collect(attValue, one);
			  		}
			  	}
			
		   }
	}  
}
  
